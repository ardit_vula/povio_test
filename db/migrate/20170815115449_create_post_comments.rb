class CreatePostComments < ActiveRecord::Migration[5.0]
  def change
    create_table :post_comments do |t|
      t.references :post, foreign_key: true
      t.integer :user_id
      t.string :comment

      t.timestamps
    end
  end
end
