require 'rails_helper'
require 'rack/test'

RSpec.describe PostsController, type: :controller do
  before { login(create(:user)) }
  before(:each) do
    @post = create(:post)
  end
  context 'posts' do
    describe 'GET /posts' do
      it 'render the posts timeline page' do
        get :index
        expect(response.status).to eq(200)
      end
    end
    describe 'GET /explore/posts' do
      it 'render the posts explore page' do
        3.times do
          (@posts ||= []) << create(:post)
        end

        get :explore
        expect(@posts.size).to eq(3)
      end
    end
  end
  describe 'POST /posts' do
    it 'responds to html by default' do
      post :create, params: { :post => { image_file_name: @post.image_file_name } }
      expect(response.content_type).to eq 'text/html'
    end

    it 'responds to custom formats when provided in the params' do
      post :create, params: { :post => { image_file_name: @post.image_file_name }, format: :json }
      expect(response.content_type).to eq 'application/json'
    end

    it 'permits only whitelisted params' do
      params = { :post => { description: '#test_description', image: Rack::Test::UploadedFile.new("spec/fixtures/test_image.png") } }

      is_expected.to permit(
                         :description,
                         :image
                     ).for(:create, params:  { params: params } ).on(:post)
    end
  end
end