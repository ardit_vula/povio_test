require 'rack/test'

FactoryGirl.define do
  factory :post do
    description '#test'
    after(:build) do |post|
      post.user = build(:user)
    end

    after(:build) do |post|
      post.image = File.open("spec/fixtures/test_image.png")
    end
  end
end
