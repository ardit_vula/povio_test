module Seeds
  def load_seeds
    load Rails.root.join('db', 'seeds.rb')
  end
end