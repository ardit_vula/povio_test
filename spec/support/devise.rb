require 'support/controllers/devise'

RSpec.configure do |config|
  config.include Warden::Test::Helpers
  config.include Devise::Test::ControllerHelpers, :type => :controller
  config.include DeviseControllerHelpers, :type => :controller

  config.after :each do
    Warden.test_reset!
  end
end