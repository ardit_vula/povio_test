module DeviseControllerHelpers
  def login(user)
    @request.env["devise.mapping"] = Devise.mappings[:user]
    user = user
    sign_in user
  end

  def login_user
    login(create(:user))
  end
end