require 'rails_helper'

RSpec.describe Post, :type => :model do
  context "valid Factory" do
    it "has a valid factory" do
      expect(build(:post)).to be_valid
    end
  end

  context "validations" do
    it "is valid with valid attributes" do
      subject = create(:post)
      expect(subject).to be_valid
    end

    it "is not valid without a image" do
      subject.user = create(:user)
      subject.description = '#beaches'
      expect(subject).to_not be_valid
    end
  end
end