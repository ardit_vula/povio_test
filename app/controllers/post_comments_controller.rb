class PostCommentsController < ApplicationController
  before_action :set_post_comment, only: [:destroy]

  # GET /post_comments/new
  def new
    @post_comment = PostComment.new
    @post_comment.post_id = params[:post_id]
  end

  # POST /post_comments
  # POST /post_comments.json
  def create
    @post_comment = PostComment.new(post_comment_params)
    @post_comment.user_id = current_user.id

    respond_to do |format|
      if @post_comment.save
        format.html { redirect_to :back, notice: 'Post comment was successfully created.' }
        format.json { render :show, status: :created, location: @post_comment }
      else
        format.html { render :new }
        format.json { render json: @post_comment.errors, status: :unprocessable_entity }
        format.js { render :new }
      end
    end
  end

  # DELETE /post_comments/1
  # DELETE /post_comments/1.json
  def destroy
    @post_comment.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Post comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_comment
      @post_comment = PostComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_comment_params
      params.require(:post_comment).permit(:post_id, :user_id, :comment)
    end
end
