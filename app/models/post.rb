class Post < ApplicationRecord
    validates_presence_of :description

    belongs_to :user
    has_many :post_likes
    has_many :post_comments

    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }

    validates :description, format: {with: /(?=.*[a-zA-Z0-9])(?=.*(#)).+/, message: 'must contain at least one hashtag'}
    validates_attachment :image, presence: true,
                         content_type: { content_type:
                                             [
                                                 'image/tiff',
                                                 'image/tif',
                                                 'image/png',
                                                 'image/jpeg',
                                                 'image/jpg'
                                             ]
                         }

    scope :not_mine, ->(user_id) {
      where.not(user_id: user_id)
    }

    def like(user_id)
      post_likes.create(user_id: user_id)
    end

    def unlike(user_id)
      post_likes.find_by(user_id: user_id).destroy
    end

    def get_likes_list
      string = ""
      post_likes.each do |like|
        string += like.user.username + ", "
      end
      string.chomp(', ') + " like this picture"
    end

    def self.popular
      popular_hash = Hash.new
      Post.all.each do |post|
        total_value = post.post_comments.size + (0.5 * post.post_likes.size)
        popular_hash[post.id] = total_value
      end

      ids = popular_hash.sort_by { |key, value| value }.reverse.map{|key,value| key}

      Post.find(ids).sort_by {|m| ids.index(m.id)}
    end
end
