class PostComment < ApplicationRecord
  default_scope { order(created_at: :desc) }

  belongs_to :post
  belongs_to :user

  validates_presence_of :comment, :post_id, :user_id
end
