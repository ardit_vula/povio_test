module PostsHelper
  def current_user_has_liked(current_user_id, post_id)
    relationship = PostLike.find_by(user_id: current_user_id, post_id: post_id)
    return true if relationship
  end
end
