Rails.application.routes.draw do
  root to: 'posts#index'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users, :controllers => { omniauth_callbacks: "users/omniauth_callbacks" }
  resources :posts
  resources :follows

  post ':id/follow_user', to: 'follows#follow_user', as: :follow_user
  post ':id/unfollow_user', to: 'follows#unfollow_user', as: :unfollow_user

  post ':id/like_post', to: 'posts#like_post', as: :like_post
  post ':id/unlike_post', to: 'posts#unlike_post', as: :unlike_post

  resources :post_comments, :except => [:index, :edit, :update, :show] do
    collection do
      get 'new/:post_id', :to => "post_comments#new", :as => 'new'
    end
  end

  get 'explore/posts', to: 'posts#explore', as: :posts_explore
  get 'profiles/:id', to: 'profile#show', as: :profile
end
